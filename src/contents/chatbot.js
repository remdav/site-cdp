export const steps = [
  {
    id: "1",
    message: "Salut, bienvenue dans cette présentation ! 😉",
    hideInput: true,
    trigger: "2",
  },
  {
    id: "2",
    options: [{ value: 0, label: "👋", trigger: "3" }],
    hideInput: true,
  },
  {
    id: "3",
    message:
      "Mon nom est Jimmy et je suis super content de vous rencontrer. 🤖",
    hideInput: true,
    trigger: "4",
  },
  {
    id: "4",
    message: "Vous êtes prêt à suivre cette présentation ? 🤗",
    hideInput: true,
    trigger: "5",
  },
  {
    id: "5",
    options: [
      { value: 0, label: "Oui, je suis prêt ! 👍", trigger: "6" },
      { value: 1, label: "Non, je me prépare ! 👎", trigger: "7" },
    ],
    hideInput: true,
  },
  {
    id: "6",
    message:
      "Génial. Comme vous le savez, mon nom est Jimmy et j'ai une semaine.. Oui j'ai seulement une semaine ! Je suis la mascotte de G4 et je suis une intelligence artificielle créée dans le but de vous présenter le cercle de projet.",
    trigger: "8",
    hideInput: true,
  },
  {
    id: "7",
    message: "Ah 😐 Vous n'êtes pas prêt, pas de soucis revenez plus tard !",
    trigger: "11",
    hideInput: true,
  },
  {
    id: "8",
    options: [
      { value: 0, label: "Wooow ! 😨 Mais c'est super cool !", trigger: "9" },
    ],
    hideInput: true,
  },
  {
    id: "9",
    message: "Pas mal hein ? 😄 Alors reste jusqu'au bout pour ne rien rater.",
    hideInput: true,
    trigger: "10",
  },
  {
    id: "10",
    options: [
      { value: 0, label: "Je ne comptais pas partir. 🙄", trigger: "12" },
    ],
    hideInput: true,
  },
  {
    id: "11",
    options: [{ value: 0, label: "C'est bon je suis prêt ! 😙", trigger: "6" }],
    hideInput: true,
  },
  {
    id: "12",
    message:
      "Okayyy, donc, le but de ce projet c'est que tu puisses intéragir avec moi via ce chat nouvel génération sur tout le site, tu pourras donc voir la présentation de Jimmy, présentation de l'équipe, la VR et les rendus téléchargeables 😜",
    hideInput: true,
    trigger: "13",
  },
  {
    id: "13",
    options: [
      {
        value: 0,
        label: "Oui c'est top mais comment on fait ça ?! 🤔",
        trigger: "14",
      },
    ],
    hideInput: true,
  },
  {
    id: "14",
    message:
      "Comment on fait ? 🤨 Haha c'est très simple, dans la page VR il te suffira juste de cliquer sur moi et de me faire tourner, dans la page d'acceuil tu verras donc ma présentation, dans la page présentation d'équipe tu pourras voir les sales têt... hm hm.. les belles têtes de mes constructeurs.. et enfin dans la page rendu il te suffira de cliquer sur les incônes pour télécharger les rendus (flyer, GIF, rendu écrit etc etc...)",
    hideInput: true,
    trigger: "15",
  },
  {
    id: "15",
    options: [
      {
        value: 0,
        label: "Oui j'ai vu, c'est énorme ! 😱",
        trigger: "16",
      },
      {
        value: 1,
        label:
          "Super je reviens dans 2 secondes, je vais aller voir les pages !",
        trigger: "17",
      },
    ],
    hideInput: true,
  },
  {
    id: "17",
    message: "Pas de problème régale toi ! 😇",
    hideInput: true,
    trigger: "18",
  },
  {
    id: "16",
    message:
      "Ah ah je te l'avais dit, maintenant tu sais tout sur moi et notre proje.. enfin le projet de mes constructeurs ! 👉👈",
    hideInput: true,
    trigger: "19",
  },
  {
    id: "18",
    options: [
      {
        value: 0,
        label: "C'est bon je suis là, elles sont vraiment bien vos pages ! 🤭",
        trigger: "16",
      },
    ],
    hideInput: true,
  },
  {
    id: "19",
    options: [
      {
        value: 0,
        label: "Euuuh.. 🙁 Tu fais pas parti du projet ?",
        trigger: "20",
      },
    ],
    hideInput: true,
  },
  {
    id: "20",
    message: "Je ne peux pas répondre à cette question désolé.",
    hideInput: true,
    trigger: "21",
  },
  {
    id: "21",
    options: [
      { value: 0, label: "Hein mais pourquoi ça ?? 😲", trigger: "22" },
    ],
    hideInput: true,
  },
  {
    id: "22",
    message: "Auto destruction activée. 👿💣",
    hideInput: true,
    trigger: "23",
  },
  {
    id: "23",
    message:
      "Destruction réussit ! 😶 Vous ne pouvez plus intéragir avec Jimmy. 🤧",
    hideInput: true,
    end: true,
  },
];
