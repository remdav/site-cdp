export const theme = {
  background: "#f5f8fb",
  fontFamily: "Arial",
  headerBgColor: "#62534F",
  headerFontColor: "#fff",
  headerFontSize: "20px",
  botBubbleColor: "#62534F",
  botFontColor: "#fff",
  userBubbleColor: "#fff",
  userFontColor: "#4a4a4a",
};
