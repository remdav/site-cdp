import React from "react";
import { Chatbot, Navbar } from "../components";
import "../styles.css";

const Enter = () => {
  return (
    <>
      <Navbar />
      <div className="textPresentation">
        <h1 className="textTitle">Qui suis-je ?</h1>
        <span className="textOne">
          Je me présente je m'appelle Jimmy et je suis un fan de JUL !
        </span>
        <img src="/jim_jul.png" className="textPicture"></img>
        <p className="textTwo">
          J'ai vu le jour le lundi 9 novembre 2020 et j'ai été créé par le
          groupe 1 pour pouvoir représenter l'Institut G4 à l'échelle nationale
          en tant que mascotte.
          <br />
          <br />
          Je suis très curieux, passionné et j'adore en apprendre davantage sur
          le monde qui m'entoure. Je suis parfois un peu trop décontracté et je
          suis un fan de l'humour. Ce sont mes constructeurs qui m'ont implanté
          ces caractéristiques dans ma tête.
          <br />
          <br />
          J'aime également le sport (oui même si je n'en ai pas l'air), la
          musique et les jeux vidéos. Tout ce qui sort de ma bouche est
          complètement objectif.
          <br />
          <br />
          Ils m'ont bien-sûr implanté énormément de louanges sur l'Institut G4
          comme le talent des professeurs ou même le génie des élèves, enfin
          surtout celui de mes constructeurs. Ils m'ont l'air un peu trop
          narcissiques et sûrs d'eux mais ils ont raison sinon je ne serais pas
          ici aujourd'hui.
        </p>
      </div>
      <Chatbot />
    </>
  );
};

export default Enter;
