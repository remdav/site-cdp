import React, { useRef } from "react";
import { useFrame } from "react-three-fiber";
import { MathUtils } from "three";

const Rig = ({ children }) => {
  const outer = useRef();
  const inner = useRef();
  useFrame(() => {
    outer.current.position.y = MathUtils.lerp(
      outer.current.position.y,
      0,
      0.05
    );
  });
  return (
    <group position={[0, -100, 0]} ref={outer}>
      <group ref={inner}>{children}</group>
    </group>
  );
};

export default Rig;
