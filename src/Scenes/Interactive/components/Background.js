import { useThree } from "react-three-fiber";
import { CubeTextureLoader } from "three";

const Background = () => {
  const { scene } = useThree();
  const loader = new CubeTextureLoader();
  const texture = loader.load([
    "/cote_1.png",
    "/cote_2.png",
    "/dessous.png",
    "/dessus.png",
    "/arriere.png",
    "/face.png",
  ]);

  scene.background = texture;
  return null;
};

export default Background;
