import { Html, Loader, OrbitControls } from "@react-three/drei";
import React, { Suspense } from "react";
import { Canvas } from "react-three-fiber";
import * as THREE from "three";
import { Navbar } from "../../components";
import { Background, Rig } from "./components";
import Model from "./Model";

export default function App() {
  return (
    <>
      <Navbar />
      <Canvas
        concurrent
        gl={{ alpha: false }}
        camera={{
          position: [0, 15, 30],
          fov: 40,
          rotation: [0, THREE.Math.degToRad(-20), THREE.Math.degToRad(180)],
        }}
      >
        <color attach="background" args={["black"]} />
        <fog attach="fog" args={["black", 10, 60]} />
        <ambientLight intensity={6} />

        <Suspense
          fallback={
            <Html center>
              <Loader />
            </Html>
          }
        >
          <Rig>
            <OrbitControls />
            <Model />
          </Rig>
          <Background />
        </Suspense>
      </Canvas>
    </>
  );
}
