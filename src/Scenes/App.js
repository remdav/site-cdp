import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { NotFoundPage } from "../components";
import { Animation, Enter, Interactive, Render, Team } from "./";

const App = () => {
  return (
    <Router>
      <Switch>
        <Route path={"/render"} component={Render} />
        <Route path={"/team"} component={Team} />
        <Route path={"/interactive"} component={Interactive} />
        <Route path={"/animation"} component={Animation} />
        <Route exact path={"/"} component={Enter} />
        <Route component={NotFoundPage} />
      </Switch>
    </Router>
  );
};

export default App;
