import { Button } from "@material-ui/core";
import CloudDownloadIcon from "@material-ui/icons/CloudDownload";
import React from "react";
import { Chatbot, Navbar } from "../components";
import "../styles.css";

const Render = () => {
  const urlSite = "https://site-cdp.vercel.app/content_render/";

  return (
    <>
      <Navbar />
      <div className={"render"}>
        <h1>Hello, c'est Jimmy</h1>
        <h3>
          Je vous présente ici les fichiers que vous pouvez télécharger
          directement via cette page.
        </h3>
        <h3>Sont disponibles : </h3>
        <h4>
          <div className={"containerPresentation"}>
            <ul>
              <li>L'explication du choix réalisé,</li>
              <li>Le flyer de la mascotte</li>
              <li>l'animation en 3D</li>
              <li>La salle de classe...</li>
            </ul>
            <img
              alt="Jimmy"
              src="Jimmy.png"
              style={{ marginLeft: "2em", width: "20em", height: "10em" }}
            />
          </div>
        </h4>
        <h4>Bonne lecture !</h4>
        <div className={"containerButton"}>
          <div className={"buttonDownload"}>
            <Button
              type="submit"
              href={`${urlSite}flyer_mockup.jpg`}
              target={"_blanck"}
            >
              <CloudDownloadIcon
                fontSize="large"
                style={{ marginRight: "20px" }}
              />
              Mockup Flyer
            </Button>
          </div>
          <div className={"buttonDownload"}>
            <Button
              type="submit"
              href={`${urlSite}flyer-recto.jpg`}
              target={"_blanck"}
            >
              <CloudDownloadIcon
                fontSize="large"
                style={{ marginRight: "20px" }}
              />
              Flyer recto
            </Button>
          </div>
          <div className={"buttonDownload"}>
            <Button
              type="submit"
              href={`${urlSite}flyer-verso.jpg`}
              target={"_blanck"}
            >
              <CloudDownloadIcon
                fontSize="large"
                style={{ marginRight: "20px" }}
              />
              Flyer verso
            </Button>
          </div>
          <div className={"buttonDownload"}>
            <Button
              type="submit"
              href={`${urlSite}rendu-equipe-1.zip`}
              target={"_blanck"}
            >
              <CloudDownloadIcon
                fontSize="large"
                style={{ marginRight: "20px" }}
              />
              dossier complet
            </Button>
          </div>
        </div>
      </div>
      <Chatbot />
    </>
  );
};
export default Render;
