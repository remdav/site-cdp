import React from "react";
import { Chatbot, Navbar } from "../../components";
import "../../styles.css";
import { Card } from "./components";

const Team = () => {
  return (
    <>
      <Navbar />
      <span className="titleTeam">Présentation de l'équipe 1</span>
      <div className="containerTeam">
        <Card
          picture={"remy.jpg"}
          name={"Rémy Damblemont"}
          job={"Chef de projet"}
        />
        <Card
          picture={"antho.jpg"}
          name={"Anthony Viot"}
          job={"Modélisateur"}
        />
        <Card picture={"jason.jpg"} name={"Jason Dangel"} job={"Développeur"} />
        <Card picture={"loris.jpg"} name={"Loris Esteve"} job={"Développeur"} />
        <Card picture={"tom.jpg"} name={"Tom Icard"} job={"Développeur"} />
        <Card picture={"guila.jpg"} name={"Guila Cohen"} job={"Développeuse"} />
        <Card
          picture={"mamadou.jpg"}
          name={"Mamadou Cisse"}
          job={"Recherche"}
        />
        <Card
          picture={"younes.jpg"}
          name={"Younes Telhaoui"}
          job={"Développeur"}
        />
      </div>
      <Chatbot />
    </>
  );
};

export default Team;
