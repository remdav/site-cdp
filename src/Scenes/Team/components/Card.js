import {
  Card as CardComponent,
  CardContent,
  CardMedia,
  Typography,
} from "@material-ui/core";
import React from "react";

const Card = ({ picture, name, job }) => {
  return (
    <>
      <CardComponent className="cardTeam">
        <CardMedia
          component="img"
          image={`/team/${picture}`}
          title="Rémy Damblemont"
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            {name}
          </Typography>
          <Typography>{job}</Typography>
        </CardContent>
      </CardComponent>
    </>
  );
};

export default Card;
