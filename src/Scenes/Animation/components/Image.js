import React from "react";
import "../../../styles.css";

const Image = ({ title, picture, alt }) => {
  return (
    <div className="contentGif">
      <span className="titleGif">{title}</span>
      <img className="imgGif" src={`/gifs/${picture}`} alt={alt} />
    </div>
  );
};

export default Image;
