import React from "react";
import { Navbar } from "../../components";
import "../../styles.css";
import { Image } from "./components";

const Animation = () => {
  return (
    <>
      <Navbar />
      <span className="typePicture">Nos GIFS</span>
      <div className="containerGif">
        <Image
          title={"Jimmy vous dit bonjour"}
          picture={"hello.gif"}
          alt={"hello_jimmy"}
        />
        <Image
          title={"Jimmy vous invite à prendre place"}
          picture={"jimmy_seat.gif"}
          alt={"seat_jimmy"}
        />
      </div>
      <div className="containerGif">
        <Image
          title={"Jimmy vous fait le signe Jul"}
          picture={"jim_jul.gif"}
          alt={"jim_jul"}
        />
      </div>
      <span className="typePicture">Nos Photos 2D</span>
      <div className="containerGif">
        <Image
          title={"Jimmy vous présente le nouveau tableau"}
          picture={"jim_prof.png"}
          alt={"jim_prof"}
        />
        <Image
          title={"Jimmy vous montre sa propre signature"}
          picture={"jimmy.png"}
          alt={"jim_sign"}
        />
      </div>
      <div className="containerGif">
        <Image
          title={"Jimmy vous montre son appartenance"}
          picture={"ag4m.png"}
          alt={"jim_ag4m"}
        />
      </div>
      <div style={{ marginTop: "4em" }} />
    </>
  );
};

export default Animation;
