import React from "react";
import Lottie from "react-lottie";
import { error404 } from "../images";
import "../styles.css";
import { Navbar } from "./";

export default function App() {
  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: error404,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice",
    },
  };

  return (
    <>
      <Navbar />
      <div>
        <Lottie options={defaultOptions} height="70%" width="70%" speed="1.6" />
        <h3 className={"titleError404"}>Page non trouvée</h3>
      </div>
    </>
  );
}
