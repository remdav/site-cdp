import {
  AppBar,
  Avatar,
  Button,
  Divider,
  Drawer,
  IconButton,
  List,
  ListItemIcon,
  ListItemText,
  Toolbar,
} from "@material-ui/core";
import BurstModeIcon from "@material-ui/icons/BurstMode";
import DescriptionIcon from "@material-ui/icons/Description";
import FindReplaceIcon from "@material-ui/icons/FindReplace";
import GroupIcon from "@material-ui/icons/Group";
import HomeIcon from "@material-ui/icons/Home";
import MenuIcon from "@material-ui/icons/Menu";
import React from "react";
import { Link } from "react-router-dom";

const Navbar = () => {
  const [open, setOpen] = React.useState(false);

  const handleDrawerOpen = () => {
    setOpen(!open);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  return (
    <>
      <AppBar position="static" style={{ backgroundColor: "#62534F" }}>
        <Toolbar>
          <IconButton
            edge="start"
            aria-label="menu"
            onClick={handleDrawerOpen}
            style={{ color: "white" }}
          >
            <MenuIcon />
          </IconButton>
        </Toolbar>
      </AppBar>
      <Drawer onClose={handleDrawerClose} anchor="left" open={open}>
        <List>
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              paddingBottom: "1em",
            }}
          >
            <Avatar src="/G4.png"></Avatar>
          </div>
          <Divider />
          <div>
            <Link to="/" style={{ color: "black", textDecoration: "none" }}>
              <Button style={{ padding: "1.5em", width: "250px" }}>
                <ListItemIcon>
                  <HomeIcon />
                </ListItemIcon>
                <ListItemText
                  style={{
                    textTransform: "initial",
                    display: "flex",
                    justifyContent: "flex-start",
                  }}
                >
                  Accueil
                </ListItemText>
              </Button>
            </Link>
          </div>
          <div>
            <Link
              to="/render"
              style={{ color: "black", textDecoration: "none" }}
            >
              <Button style={{ padding: "1.5em", width: "250px" }}>
                <ListItemIcon>
                  <DescriptionIcon />
                </ListItemIcon>
                <ListItemText
                  style={{
                    textTransform: "initial",
                    display: "flex",
                    justifyContent: "flex-start",
                  }}
                >
                  Nos rendus
                </ListItemText>
              </Button>
            </Link>
          </div>
          <div>
            <Link to="/team" style={{ color: "black", textDecoration: "none" }}>
              <Button style={{ padding: "1.5em", width: "250px" }}>
                <ListItemIcon>
                  <GroupIcon />
                </ListItemIcon>
                <ListItemText
                  style={{
                    textTransform: "initial",
                    display: "flex",
                    justifyContent: "flex-start",
                  }}
                >
                  Equipe
                </ListItemText>
              </Button>
            </Link>
          </div>
          <div>
            <Link
              to="/animation"
              style={{ color: "black", textDecoration: "none" }}
            >
              <Button style={{ padding: "1.5em", width: "250px" }}>
                <ListItemIcon>
                  <BurstModeIcon />
                </ListItemIcon>
                <ListItemText
                  style={{
                    textTransform: "initial",
                    display: "flex",
                    justifyContent: "flex-start",
                  }}
                >
                  Galerie
                </ListItemText>
              </Button>
            </Link>
          </div>
          <div>
            <a
              href="https://site-cdp.vercel.app/interactive"
              style={{ color: "black", textDecoration: "none" }}
            >
              <Button style={{ padding: "1.5em", width: "250px" }}>
                <ListItemIcon>
                  <FindReplaceIcon />
                </ListItemIcon>
                <ListItemText
                  style={{
                    textTransform: "initial",
                    display: "flex",
                    justifyContent: "flex-start",
                  }}
                >
                  Réalité augmentée
                </ListItemText>
              </Button>
            </a>
          </div>
        </List>
      </Drawer>
    </>
  );
};

export default Navbar;
