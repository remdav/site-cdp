import React, { useState } from "react";
import ChatBot from "react-simple-chatbot";
import { ThemeProvider } from "styled-components";
import { steps } from "../contents/chatbot";
import { theme } from "../themes/chatbot";

const Chatbot = () => {
  const [isOpenChat, setIsOpenChat] = useState(false);

  const openChat = () => {
    setIsOpenChat(!isOpenChat);
  };

  return (
    <>
      <div className="containerCircleChat">
        <div className="chatbot" onClick={openChat}>
          <img className="imageChat" src="/chatbot.png" alt={"chatbox"} />
        </div>
      </div>
      {isOpenChat && (
        <ThemeProvider theme={theme}>
          <div className="ContainerChat">
            <ChatBot
              steps={steps}
              headerTitle={"Jimmy"}
              userAvatar={"/G4.png"}
              opened={true}
              hideSubmitButton={true}
            />
          </div>
        </ThemeProvider>
      )}
    </>
  );
};

export default Chatbot;
